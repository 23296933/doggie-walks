// Declare variables
var modal = document.getElementById('myModal');
var images = document.getElementsByClassName('galleryImage');
var modalImg = document.getElementById("img01");

// Loops through all the images in the class
for (var i = 0; i < images.length; i++) {
  var img = images[i];
  // When an image is clicked, the modal opens
  img.onclick = function(evt) {
    console.log(evt);
    modal.style.display = "block";
    modalImg.src = this.src;
  }
}
// Declare variable for the close button
var span = document.getElementsByClassName("close")[0];
// when the close button is clicked, removes modal
span.onclick = function() {
  modal.style.display = "none";
}
// when the modal is clicked, removes modal
modal.onclick = function() {
	modal.style.display = "none";
}